New:
 - x-axis for weekly steps graph now shows full date of first day of week

Bug fixes:
 - fixed regression that stopped taps on notification opening app


