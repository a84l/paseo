package ca.chancehorizon.paseo


import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper

import kotlinx.android.synthetic.main.dashboard_steps_details_bottomsheet.*
import java.text.NumberFormat


class DashboardStepsDetailsFragment : BottomSheetDialogFragment(), View.OnClickListener {


    var timeUnit: String = "day"
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var timeUnitSummaryTitle: String = "Summary for..."
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var steps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var expectedSteps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var projectedSteps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var targetSteps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var timesOnTarget: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var averageSteps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var maximumSteps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var minimumSteps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val theTheme = requireContext().getTheme()
        val contextThemeWrapper = ContextThemeWrapper(activity, theTheme)

        return inflater.cloneInContext(contextThemeWrapper).inflate(R.layout.dashboard_steps_details_bottomsheet, container, false)

    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // respond to the user tapping anywhere in the dialog
        legendLayout.setOnClickListener(this)

        // fill in the details for this timeUnit
        updateDetail()
    }

//    override fun onAttach(context: Context?) {
//        super.onAttach(context)
//    }
//
//    override fun onDetach() {
//        super.onDetach()
//    }



    fun updateDetail() {

        detailTimeUnitSummaryTitle.text = timeUnitSummaryTitle
        detailProjectedSteps.text = timeUnit
        detailSteps.text = NumberFormat.getIntegerInstance().format(steps)
        detailExpectedSteps.text = NumberFormat.getIntegerInstance().format(expectedSteps)
        detailProjectedSteps.text = NumberFormat.getIntegerInstance().format(projectedSteps)
        detailTargetSteps.text = NumberFormat.getIntegerInstance().format(targetSteps)

        detailAllTimeSummaryTitle.text = getString(R.string.summary_all) + timeUnit.capitalize() + "s"
        timesOnTargetLabel.text = " " + timeUnit.capitalize() + getString(R.string.on_target)
        detailTimesOnTarget.text = timesOnTarget.toString()
        detailAverageSteps.text = NumberFormat.getIntegerInstance().format(averageSteps)
        detailMaximumSteps.text = NumberFormat.getIntegerInstance().format(maximumSteps)
        detailMinimumSteps.text = NumberFormat.getIntegerInstance().format(minimumSteps)
    }



    // respond to user taps
    override fun onClick(view: View) {

        // close the dialog
        dismiss()
    }



    // *** can remove ???
    interface ItemClickListener {
        fun onItemClick(item: String)
    }


    //  *** can remove ???
    companion object {

        val TAG = "ActionBottomDialog"

        fun newInstance(): DashboardStepsDetailsFragment {
            return DashboardStepsDetailsFragment()
        }
    }
}