package ca.chancehorizon.paseo

import android.Manifest
import android.app.ActivityManager
import android.content.*
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Color
import android.hardware.SensorManager
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import ca.chancehorizon.paseo.background.StepCounterService
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import java.util.*
import android.graphics.PorterDuff

import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import androidx.core.graphics.drawable.DrawableCompat
import androidx.appcompat.content.res.AppCompatResources








class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, TextToSpeech.OnInitListener  {

    var sensorManager: SensorManager? = null
    var theActivityManager: ActivityManager? = null

    var startSteps = 0
    var lastStepDate = 0

    var theScreen = "dashboard"

    private var shouldRecreate = false

    lateinit var paseoDBHelper : PaseoDBHelper

    // get the application settings (save messages etc)
    lateinit var paseoPrefs : SharedPreferences

    private val SETTINGS_RESULT = 1

    private var tts: TextToSpeech? = null
    private var ttsAvailable = false

    private val TAG = "Permission Request"
    private val ACTIVITY_REQUEST_CODE = 101


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // switch to the theme that the user has set in the settings
        changeTheme()

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        // enable the opening and closing of the navigation drawer (menu)
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        // enable actions resulting from user selecting items in the navigation menu
        nav_view.setNavigationItemSelectedListener(this)

        // make sure all the menu item icons are the same color
        for (theMenuItem in 0 until nav_view.menu.size()) {
            val theMenuIcon: Drawable = nav_view.menu.getItem(theMenuItem).icon
            // only set the color if there actually is an icon for this menu item
            if (theMenuIcon != null) {
                //theMenuIcon.mutate()
                //theMenuIcon.setColorFilter(resolveColorAttr(this, android.R.attr.textColorPrimary), PorterDuff.Mode.SRC_IN)
            }
        }

        // request user permission for paseo to access required sensors
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            setupPermissions()
        }

        // set up the manager for the step counting service
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        // set up the manager for the activity detection service
        //theActivityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        supportActionBar!!.setDisplayShowHomeEnabled(true)

        // point to the Paseo database that stores all the daily steps data
        paseoDBHelper = PaseoDBHelper(this)

        // get the date of the last record from the steps table in the database
        lastStepDate = paseoDBHelper.readLastStepsDate()

        // get the start steps of the last record from the steps table in the database
        startSteps = paseoDBHelper.readLastStartSteps()

        // display the dashboard when Paseo launches
        if (savedInstanceState == null) {
            // The Activity is being created for the first time
            val fragment = DashboardFragment()
            displaySelectedFragment(fragment, "dashboard")
            nav_view.getMenu().getItem(0).setChecked(true)
        }

        // respond to the user tapping on the help icon (question mark)
        helpIcon.setOnClickListener { view ->

            var helpView : View = layoutInflater.inflate(R.layout.dashboard_help_bottomsheet, null)

            // respond to user tapping on the help icon "?".
            when (theScreen) {
                "dashboard" -> {
                    // show the dashboard help dialog sheet
                    helpView = layoutInflater.inflate(R.layout.dashboard_help_bottomsheet, null)
                }
                "miniGoals" -> {
                    // show the dashboard help dialog sheet
                    helpView = layoutInflater.inflate(R.layout.mini_goal_help_bottomsheet, null)
                }
                "records" -> {
                    // show the dashboard help dialog sheet
                    helpView = layoutInflater.inflate(R.layout.records_help_bottomsheet, null)
                }
                "hours", "days", "weeks", "months", "years" -> {
                    // show the dashboard help dialog sheet
                    helpView = layoutInflater.inflate(R.layout.step_summary_help_bottomsheet, null)
                }
            }

            val dialog = BottomSheetDialog(this)
            dialog.setContentView(helpView)
            dialog.show()
        }
    }



    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACTIVITY_RECOGNITION)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACTIVITY_RECOGNITION)) {
                val builder = AlertDialog.Builder(this)
                builder.setMessage(R.string.activity_permission_last_warning)
                    .setTitle(R.string.permission_warning_title)

                builder.setPositiveButton("OK") { dialog, id ->
                    makeRequest()
                }

                val dialog = builder.create()
                dialog.show()
            } else {
                makeRequest()
            }
        }
    }



    private fun makeRequest() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACTIVITY_RECOGNITION), ACTIVITY_REQUEST_CODE)
    }



    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            ACTIVITY_REQUEST_CODE -> {
                val paseoPrefs = this.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

                // update shared preferences to not restart the step counting service
                val edit: SharedPreferences.Editor = paseoPrefs!!.edit()

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission has been denied by user")

                    stopServices()
                    super.onBackPressed()

                } else {
                    Log.i(TAG, "Permission has been granted by user")

                    edit.putBoolean("prefRestartService", true)
                    edit.apply()
                }
            }
        }
    }



    override fun onStart() {
        super.onStart()

        // do not start the step counting service if it is already running
        if (!isServiceRunning("ca.chancehorizon.paseo.background.StepCounterService")) {
            startStepService()
        }
    }



    override fun onInit(status: Int) {

        val paseoPrefs = this.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        // set up the text to speech voice
        if (status == TextToSpeech.SUCCESS) {
            ttsAvailable = true
        }
        else {
            Log.e("TTS", "Initialization failed")
            ttsAvailable = false
        }

        // update shared preferences to not show first run dialog again
        val edit: SharedPreferences.Editor = paseoPrefs!!.edit()
        edit.putBoolean("prefTTSAvailable", ttsAvailable)
        // default the first day of week to the one from the device's current Locale (could be Sunday or Monday)contains(String key)
        if (!paseoPrefs.contains("prefFirstDayOfWeek")) {
            edit.putString(
                "prefFirstDayOfWeek",
                Calendar.getInstance(Locale.getDefault()).firstDayOfWeek.toString()
            )
        }

        edit.apply()
    }



    override fun onDestroy() {
        stopServices()

        val permission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACTIVITY_RECOGNITION)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            val parentLayout = findViewById<View>(android.R.id.content)
            Snackbar.make(parentLayout, "Paseo has not been granted permission to read the devices step sensor.", Snackbar.LENGTH_LONG)
                .setAction("CLOSE") { }
                .setActionTextColor(resources.getColor(android.R.color.holo_red_light))
                .show()
        }


        // Shutdown TTS
        if (tts != null) {
            tts!!.stop()
            tts!!.shutdown()
        }


        super.onDestroy()
    }



    override fun onResume() {
        super.onResume()

        // get the application settings (save messages etc)
        val paseoPrefs = this.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
        val firstRun = paseoPrefs!!.getBoolean("prefFirstRun", true)
        theScreen = paseoPrefs.getString("prefLastTimeUnit", "dashboard")!!

        // show a welcome message to users that have launched paseo for the first time
        if (firstRun) {
            val view: View = layoutInflater.inflate(R.layout.paseo_welcome_bottomsheet, null)

            // welcome message is shown in bottom sheet dialog
            val aboutPaseoBottomSheet = BottomSheetDialog(this)
            aboutPaseoBottomSheet.setContentView(view)
            aboutPaseoBottomSheet.show()
            val bottomSheet = aboutPaseoBottomSheet.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

            // update shared preferences to not show first run dialog again
            val edit: SharedPreferences.Editor = paseoPrefs.edit()
            edit.putBoolean("prefFirstRun", false)
            edit.apply()
        }

        // redraw the entire screen (used to change the theme when returning from settings)
        if (shouldRecreate) {
            recreate()

            val timeUnit = paseoPrefs.getString("prefLastTimeUnit", "days")

            // only do this for the step summary fragment (actual time unit, rather than something else)
            if (timeUnit in arrayOf("hours", "days", "weeks", "months", "years")) {
                val fragment: androidx.fragment.app.Fragment?
                fragment = StepSummaryFragment()
                fragment.timeUnit = timeUnit!!
                displaySelectedFragment(fragment, timeUnit)
            }

            shouldRecreate = false
        }
    }



    override fun onPause () {
        super.onPause()

        shouldRecreate = true
    }



    // react to user selection in navigation menu
    override fun onNavigationItemSelected(item: MenuItem): Boolean
    {
        var fragment: androidx.fragment.app.Fragment?

        // respond to user tapping items in the navigation menu.
        when (item.itemId) {
            R.id.nav_dashboard -> {
                fragment = DashboardFragment()
                theScreen = "dashboard"
                displaySelectedFragment(fragment, theScreen)
            }
            R.id.nav_steps_hour -> {
                theScreen = "hours"
            }
            R.id.nav_steps_day -> {
                theScreen = "days"
            }
            R.id.nav_steps_week -> {
                theScreen = "weeks"
            }
            R.id.nav_steps_month -> {
                theScreen = "months"
            }
            R.id.nav_steps_year -> {
                theScreen = "years"
            }
            R.id.nav_mini_goals -> {
                fragment = MiniGoalFragment()
                theScreen = "miniGoals"
                displaySelectedFragment(fragment, theScreen)
            }
            R.id.nav_records -> {
                fragment = RecordsFragment()
                theScreen = "records"
                displaySelectedFragment(fragment, theScreen)
            }
            R.id.nav_settings -> {
                val i = Intent(this, SetPreferencesActivity::class.java)
                theScreen = "settings"
                startActivityForResult(i, SETTINGS_RESULT)
            }
/*
            R.id.nav_recorded_activity -> {
                fragment = ActivitiesListFragment()
                theScreen = "activities"
                displaySelectedFragment(fragment, theScreen)
            }
*/
            // user has selected quit from menu - exit the application
            R.id.nav_quit -> {

                // *** might want to give user the option to keep step counting service running
                stopServices()
                finish()
            }
        }

        // save the selected item to the shared preference so that it will be redisplayed when
        //  returning from settings
        if (theScreen != "settings") {
            val paseoPrefs = this.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
            val edit: SharedPreferences.Editor = paseoPrefs.edit()
            edit.putString("prefLastTimeUnit", theScreen)
            edit.apply()
        }

        // show the time unit step summary screen for the user select time unit
        if (theScreen in arrayOf("hours", "days", "weeks", "months", "years")) {
            fragment = StepSummaryFragment()
            fragment.timeUnit = theScreen
            displaySelectedFragment(fragment, theScreen)
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }



    /**
     * Loads the specified fragment to the frame
     *
     * @param fragment
     */
    private fun displaySelectedFragment(fragment : androidx.fragment.app.Fragment, fragmentTag : String) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame, fragment, fragmentTag)
        fragmentTransaction.commit()
    }



    // if the user pressed the back button, ask to confirm quitting paseo
    //  - except if the drawer is open, close the drawer (no quitting)
    //  - or return to the dashboard from one of the step summary screens
    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        else {

            val currentFragment = supportFragmentManager.fragments.last()

            // offer to quit paseo if the user is looking at the dashboard
            if (currentFragment.getTag() == "dashboard") {

                // set up the quit alert with option to cancel (not quit)
                val quitDialog = AlertDialog.Builder(this, R.style.PaseoDialog2)
                        .setTitle("Really Exit?")
                        .setMessage("Are you sure you want to exit?")
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, DialogInterface.OnClickListener { dialog, which ->
                            super.onBackPressed()
                        })
                quitDialog.create()
                quitDialog.show()
            }
            // otherwise, switch back to the dashboard
            else {
                // make sure the dashboard item in the menu is selected
                nav_view.getMenu().getItem(0).setChecked(true)
                // display the dashboard
                displaySelectedFragment(DashboardFragment(), "dashboard")

                theScreen = "dashboard"
            }
        }
    }



    // check if the step counting service is already running (to avoid starting a second one)
    private fun isServiceRunning(serviceName: String): Boolean {
        var serviceRunning = false
        val theActivityManager = this.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val runningServices = theActivityManager.getRunningServices(50)
        val eachService: Iterator<ActivityManager.RunningServiceInfo> = runningServices.iterator()

        // loop through the running services
        while (eachService.hasNext()) {
            val runningServiceInfo = eachService
                    .next()

            // check if this service's name for a match to the one passed in to this function as
            //  an argument (will most commonly be paseo's step counting service)
            if (runningServiceInfo.service.className == serviceName) {
                serviceRunning = true

            }
        }

        // if true, the service was found running
        return serviceRunning
    }



    // start the step counting and activity detection services
    private fun startStepService() {
        startService(Intent(this, StepCounterService::class.java))
    }



    // stop the step counting service and activity detection services
    private fun stopServices() {
        // stop the step counting service
        stopService(Intent(this, StepCounterService::class.java))
    }



    // executed when the user has selected quit from the menu
    override fun finish()
    {
        super.finish()

        // without this line, Paseo will be closed, but still running.
        System.exit(0)
    }



    // switch to the user selected theme (run at launch and every time the user leaves the settings screen)
    fun changeTheme()
    {
        // get the application settings (save messages etc)
        paseoPrefs = this.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        val theme = paseoPrefs.getString("prefTheme", "1")!!.toInt()
        // enable randomly setting the theme (if user selected random in the settings)
        val themeSetting = if (theme <= 10) theme else (Math.random()*10).toInt()

        // set the correct theme here
        //  (need to (re)create the activity in order for theme change complete (be seen)
        //  done on "onCreate" at launch and "onResume" when returning from settings)
        when (themeSetting) {
            1 -> {
                setTheme(R.style.Theme_Paseo_Blue)
            }
            2 -> {
                setTheme(R.style.Theme_Paseo_Green)
            }
            3 -> {
                setTheme(R.style.Theme_Paseo_Red)
            }
            4 -> {
                setTheme(R.style.Theme_Paseo_Orange)
            }
            5 -> {
                setTheme(R.style.Theme_Paseo_Purple)
            }
            6 -> {
                setTheme(R.style.Theme_Paseo_Pink)
            }
            7 -> {
                setTheme(R.style.Theme_Paseo_Yellow)
            }
            8 -> {
                setTheme(R.style.Theme_Paseo_Brown)
            }
            9 -> {
                setTheme(R.style.Theme_Paseo_Grey)
            }
            10 -> {
                setTheme(R.style.Theme_Paseo_BlueGrey)
            }
        }

        val base = paseoPrefs.getString("prefThemeBase", "1")!!.toInt()

        when (base) {
            1 -> {
                // no need to change anything as default is the light base
            }
            2 -> {
                getTheme().applyStyle(R.style.OverlayDark, true)
            }
            3 -> {
                getTheme().applyStyle(R.style.OverlayBlack, true)
            }
        }
    }
}



class BootUpReceiver : BroadcastReceiver()
{
    override fun onReceive(context: Context, intent: Intent)
    {
        val i = Intent(context, MainActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(i)
    }
}



// Extension function to show toast message
fun Context.toast(message:String){
    Toast.makeText(applicationContext,message, Toast.LENGTH_LONG).show()
}



// return the date of the first day in the current week (could be the most recent Sunday or Monday)
fun getFirstDayInWeek(weekStart: Int, theYear: Int = 0, theMonth: Int = 0, theDay: Int = 0): Date {

    var theCalendar = Calendar.getInstance(Locale.UK)

    // use a CANADA calendar so that start of week is Sunday
    if (weekStart == 1) {
        theCalendar = Calendar.getInstance(Locale.CANADA)
    }

    if (theYear != 0) {
        theCalendar.set(theYear, theMonth - 1, theDay)

    }
    // get start of this week in milliseconds
    while (theCalendar.get(Calendar.DAY_OF_WEEK) != weekStart) {
        theCalendar.add(Calendar.DATE, -1)
    }

    theCalendar[Calendar.HOUR_OF_DAY] = 0 // ! clear would not reset the hour of day !
    theCalendar.clear(Calendar.MINUTE)
    theCalendar.clear(Calendar.SECOND)
    theCalendar.clear(Calendar.MILLISECOND)

    return theCalendar.time
}



fun getDayOfWeekNumber(date: Date, weekStart: Int): Int {

    // default to Sunday as first day of week
    val theCalendar = Calendar.getInstance()
    theCalendar.time = date
    var dayOfWeek = theCalendar[Calendar.DAY_OF_WEEK]

    // set first day of week to Monday if set by user (in app settings)
    if (weekStart == 2) {
        dayOfWeek = dayOfWeek - 1
        // when user has set week start on Monday and the Locale week start is Sunday,
        //   dayOfWeek for Sundays end up as day 0 (no good - division by zero later)
        //  need to set Sunday to day 7 (day of rest - maybe user should not take any steps at all)
        if (dayOfWeek < 1) {
            dayOfWeek = 7
        }
    }

    return dayOfWeek
}



// get the day of week number of the first day of week based on the currently set Locale of the device
fun getLocaleFirstDayOfWeek(): Int {
    val theCalendar = Calendar.getInstance(Locale.getDefault())
    return theCalendar.firstDayOfWeek
}



// determine the number of days in the month
fun getDaysInMonth(theYear:Int, theMonth:Int = 0, theDay:Int = 0) : Int {
    val theCalendar = Calendar.getInstance()

    // use today's date if -1 is passed as the year
    if(theYear == -1){
        theCalendar.time = Date()
    }
    else {
        theCalendar.set(theYear, theMonth, theDay)
    }

    return theCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)
}



// determine if the year passed to the function is a leap year
fun isLeapYear(theYear: Int): Boolean {

    return when {
        theYear % 4 == 0 -> {
            when {
                theYear % 100 == 0 -> theYear % 400 == 0
                else -> true
            }
        }
        else -> false
    }
}



// get the color set by the current theme for specific color references
fun Context.getThemeColor(
        @AttrRes attrColor: Int,
        typedValue: TypedValue = TypedValue(),
        resolveRefs: Boolean = true
): Int {
    theme.resolveAttribute(attrColor, typedValue, resolveRefs)
    return typedValue.data
}



// get theme details
//  used by getThemeColor
fun resolveThemeAttr(context: Context, @AttrRes attrRes: Int): TypedValue {
    val theme = context.theme
    val typedValue = TypedValue()
    theme.resolveAttribute(attrRes, typedValue, true)
    return typedValue
}



@ColorInt
fun resolveColorAttr(context: Context, @AttrRes colorAttr: Int): Int {
    val resolvedAttr = resolveThemeAttr(context, colorAttr)
    // resourceId is used if it's a ColorStateList, and data if it's a color reference or a hex color
    val colorRes = if (resolvedAttr.resourceId != 0)
        resolvedAttr.resourceId
    else
        resolvedAttr.data
    return ContextCompat.getColor(context, colorRes)
}
